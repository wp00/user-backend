import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
let products: Product[] = [
  { id: 1, name: 'coffeeee', price: 60 },
  { id: 2, name: 'Espresso', price: 75 },
  { id: 3, name: 'Flat White', price: 85 },
];
let lastProductId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    {
      const newProduct: Product = {
        id: lastProductId++,
        ...createProductDto,
      };
      products.push(newProduct);
      return newProduct;
    }
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user ' + JSON.stringify(users[index]));
    // console.log('update ' + JSON.stringify(updateUserDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }
  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'coffee', price: 60 },
      { id: 2, name: 'Espresso', price: 75 },
      { id: 3, name: 'Flat White', price: 85 },
    ];
    lastProductId = 0;
    return 'RESET';
  }
}
